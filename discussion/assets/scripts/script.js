// while loop
/*function whileLoop(){
	let count = 5;

	while(count != 0) {
		console.log('while iterration count: ' + count);
		count--;
	}
}

whileLoop();*/

function whileLoop(){
	let count = 5;

	while(count != 0) {
		console.log('while iterration count: ' + count);
		count--;
	}

	/*let loginAttempt = 3;
	while(loginAttempt != 0) {
		alert('invalid login');
		timer--;
	}*/
}

whileLoop();

// do while loop
function doWhileLoop() {
	let count = 20;

	do {
		console.log('dowhile iterration count: ' + count)
		count--;
	} while (count > 0);
}

doWhileLoop();

//  for loop

function forLoop() {
	/*
	for (initilization; condition; increment/decrement) {
		block of code goes here
	}
	*/

	for (let count = 0; count <= 20; count++) {
		console.log('forLoop iteration count: ' + count);
	}
}

forLoop();

/*let vote = 0;

for (let names = 10; names >= 0; names--) {
	vote += 1;
}*/


// forEach

let namesArray = ['john', 'jane', 'joe', 'bill'];

function sayHello(){
	console.log('hello');
}

namesArray.forEach(sayHello);

// ========================================================
// continue and break
// break - used to stop loops
// continue - tells the loop to continue to the next iteration, ignores all code after the continue statement

function modifiedForLoop() {
	for (let count = 0; count <= 20; count++) {
		if (count % 2 === 0) {
			console.log('count ' + count);
			continue;
		}

		console.log(count);

		if (count >= 10) {
			console.log(count);
			break;
		}
	}
}

modifiedForLoop();


/*let number = parseInt(prompt('Enter an integer: '));


for(let i = 1; i <= 10; i++) {

    result = i * number;

    console.log(`${number} * ${i} = ${result}`);
}
*/
function multiplicationMatrix() {
	for (let count = 1; count <= 10; count++) {
		console.log(count);
		for(let i = 1; i <= 10; i++) {

    	result = i * count;
    	console.log(`${count} * ${i} = ${result}`);
		}
	}

}

multiplicationMatrix();